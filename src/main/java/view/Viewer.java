package view;

import model.Board;
import model.Player;

public class Viewer {

    public void displayBoards(Player player) {
        Board myBoard = player.getMyBoard();
        Board enemyBoard = player.getEnemyBoard();

        System.out.println();
        System.out.println(player.getName() + "'s boards:            " + player.getName() + "'s opponent:");
        System.out.println("   A  B  C  D  E  F  G  H  I  J        A  B  C  D  E  F  G  H  I  J");
        for (int j = 1; j < myBoard.getSize() - 1; j++) {
            if (j > 9) {
                System.out.print(j + "|");
            } else {
                System.out.print(j + " |");
            }
            for (int i = 1; i < myBoard.getSize() - 1; i++) {
                System.out.print(myBoard.getBoard()[j][i] + "  ");
            }
            if (j > 9) {
                System.out.print("   " + j + "|");
            } else {
                System.out.print("   " + j + " |");
            }
            for (int i = 1; i < myBoard.getSize() - 1; i++) {
                System.out.print(enemyBoard.getBoard()[j][i] + "  ");
            }
            System.out.println();
        }
    }
}
