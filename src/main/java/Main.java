import controller.Controller;
import model.Player;
import view.Viewer;

public class Main {

    public static void main(String[] args) {
        Viewer viewer = new Viewer();
        Controller controller = new Controller(viewer);
        Player player1 = controller.addPlayer("Kpt. Kowalski", false);
        Player player2 = controller.addPlayer("Cpt. Nelson", true);

        controller.deployShipsInRandom(player1.getMyBoard());
        controller.deployShipsInRandom(player2.getMyBoard());
        viewer.displayBoards(player1);

        do {
            System.out.println("Provide coordinates to shoot at:");
            controller.shoot(player1, player2);
            controller.shoot(player2, player1);
            viewer.displayBoards(player1);
            System.out.println(player1.getName() + " has " + player1.getHp() + " HP.");
            System.out.println(player2.getName() + " has " + player2.getHp() + " HP.");
        } while (player1.getHp() > 0 && player2.getHp() > 0);

        System.out.println("Battle is over!");
        System.out.println("Result:");
        System.out.println(player1.getName() + " " + player1.getHp());
        System.out.println(player2.getName() + " " + player2.getHp());

    }
}
