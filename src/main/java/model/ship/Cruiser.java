package model.ship;

import controller.Setting;
import model.Board;

public class Cruiser extends Ship {
    public Cruiser(boolean orientation, Board board) {
        super(Setting.SIZE_OF_CRUISER, "cruiser", orientation, board);
    }
}
