package model.ship;

import model.Board;

import java.util.ArrayList;
import java.util.List;

public abstract class Ship {
    private int size;
    private int hp;
    private boolean isAlive;
    private boolean orientation;
    private String type;
    private Board board;
    List<int[]> myCoordinates = new ArrayList<>();


    public void placeShip(int x, int y) {
        if (isEnoughSpace(x, y)) {
            if (orientation == true) {
                for (int i = -1; i < size + 1; i++) {
                    board.getBoard()[x + i][y - 1] = ".";
                    board.getBoard()[x + i][y] = ".";
                    board.getBoard()[x + i][y + 1] = ".";
                }
                for (int i = 0; i < size; i++) {
                    board.getBoard()[x + i][y] = "O";
                    myCoordinates.add(new int[]{x + i, y});
                }
                board.getListOfDeployedShips().add(this);
            } else {
                for (int i = -1; i < size + 1; i++) {
                    board.getBoard()[x - 1][y + i] = ".";
                    board.getBoard()[x][y + i] = ".";
                    board.getBoard()[x + 1][y + i] = ".";
                }
                for (int i = 0; i < size; i++) {
                    board.getBoard()[x][y + i] = "O";
                    myCoordinates.add(new int[]{x, y + i});
                }
                board.getListOfDeployedShips().add(this);
            }
        } else {
            System.out.println("Cannot deploy at: " + x + " " + y);
        }
    }

    public boolean isEnoughSpace(int x, int y) {
        if (orientation == true) {
            try {
                for (int i = 0; i < size; i++) {
                    if (board.getBoard()[x + i][y].equals(" ")) {
                    } else {
                        return false;
                    }
                }
                for (int i = -1; i < size + 1; i++) {
                    if (board.getBoard()[x + i][y - 1].equals("O")) {
                        return false;
                    }
                }
                for (int i = -1; i < size + 1; i++) {
                    if (board.getBoard()[x + i][y].equals("O")) {
                        return false;
                    }
                }
                for (int i = -1; i < size + 1; i++) {
                    if (board.getBoard()[x + i][y + 1].equals("O")) {
                        return false;
                    }
                }
            } catch (Exception e) {
                return false;
            }
        } else {
            try {
                for (int i = 0; i < size; i++) {
                    if (board.getBoard()[x][y + i].equals(" ")) {
                    } else {
                        return false;
                    }
                }
                for (int i = -1; i < size + 1; i++) {
                    if (board.getBoard()[x - 1][y + i].equals("O")) {
                        return false;
                    }
                }
                for (int i = -1; i < size + 1; i++) {
                    if (board.getBoard()[x][y + i].equals("O")) {
                        return false;
                    }
                }
                for (int i = -1; i < size + 1; i++) {
                    if (board.getBoard()[x + 1][y + i].equals("O")) {
                        return false;
                    }
                }
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    public int getSize() {
        return size;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
        if (this.hp < 1) {
            setAlive(false);
        }
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public String getName() {
        return type;
    }

    public Ship(int size, String type, boolean orientation, Board board) {
        this.size = size;
        this.hp = size;
        this.isAlive = true;
        this.orientation = orientation;
        this.type = type;
        this.board = board;
    }

    public List<int[]> getMyCoordinates() {
        return myCoordinates;
    }

    public boolean getOrientation() {
        return orientation;
    }
}