package model.ship;

import controller.Setting;
import model.Board;

public class BattleShip extends Ship {
    public BattleShip(boolean orientation, Board board) {
        super(Setting.SIZE_OF_BATTLESHIP, "battleship", orientation, board);
    }
}
