package model.ship;

import controller.Setting;
import model.Board;

public class Gunboat extends Ship {
    public Gunboat(boolean orientation, Board board) {
        super(Setting.SIZE_OF_GUNBOAT, "gunboat", orientation, board);
    }
}
