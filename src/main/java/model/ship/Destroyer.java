package model.ship;

import controller.Setting;
import model.Board;

public class Destroyer extends Ship {
    public Destroyer(boolean orientation, Board board) {
        super(Setting.SIZE_OF_DESTROYER, "destroyer", orientation, board);
    }
}
