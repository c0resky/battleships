package model;

import controller.Setting;
import model.ship.Ship;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private int size = Setting.BOARD_SIZE + 2;
    private String [][] board = new String[size][size];
    private List<Ship> listOfDeployedShips = new ArrayList<Ship>();

    public String[][] getBoard() {
        return board;
    }

    public int getSize() {
        return size;
    }

    public List<Ship> getListOfDeployedShips() {
        return listOfDeployedShips;
    }
}
