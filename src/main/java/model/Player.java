package model;

import controller.Setting;

public class Player {

    private String name;
    private Board myBoard;
    private Board enemyBoard;
    private int hp = Setting.NUMBER_OF_GUNBOATS * Setting.SIZE_OF_GUNBOAT +
            Setting.NUMBER_OF_DESTROYERS * Setting.SIZE_OF_DESTROYER +
            Setting.NUMBER_OF_CRUISERS * Setting.SIZE_OF_CRUISER +
            Setting.NUMBER_OF_BATTLESHIPS * Setting.SIZE_OF_BATTLESHIP;
    boolean isComputerControlled;

    public Player(String name, boolean isComputerControlled) {
        this.name = name;
        this.myBoard = new Board();
        this.enemyBoard = new Board();
        this.isComputerControlled = isComputerControlled;
    }

    public Board getMyBoard() {
        return myBoard;
    }

    public Board getEnemyBoard() {
        return enemyBoard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean isComputerControlled() {
        return isComputerControlled;
    }

}
