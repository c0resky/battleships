package controller;

import model.Board;
import model.Player;
import model.ship.*;
import view.Viewer;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Controller {

    private Viewer viewer;
    private Scanner scanner = new Scanner(System.in);
    private Random random = new Random();

    private Board cleanBoard(Board board) {

        int size = board.getSize();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                board.getBoard()[i][j] = " ";
            }
        }
        return board;
    }

    public Player addPlayer(String name, boolean isComputerControlled) {
        Player player = new Player(name, isComputerControlled);
        cleanBoard(player.getMyBoard());
        cleanBoard(player.getEnemyBoard());
        return player;
    }

    public String scanLine() {
        return scanner.nextLine();
    }

    public int convertToYCoordinate(String line) {
        try {
            line = line.toUpperCase();
            char character = line.charAt(0);
            return character - 64;
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
        return 0;
    }

    public int convertToXCoordinate(String line) {
        try {
            line = line.replaceAll("\\s", "");
            line = line.substring(1);
            if (line.length() == 1 || line.length() == 2) {
                try {
                    return Integer.parseInt(line);
                } catch (Exception e) {
                    return 0;
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
        return 0;
    }

    public boolean isInputCorrect(int x, int y) {

        if (x > 0 && x < Setting.BOARD_SIZE + 1 && y > 0 && y < Setting.BOARD_SIZE + 1) {
            return true;
        }
        return false;
    }

    public void deployShipsInRandom(Board board) {

        for (int i = 0; i < Setting.NUMBER_OF_BATTLESHIPS; i++) {
            deployShip(new BattleShip(random.nextBoolean(), board));
        }

        for (int i = 0; i < Setting.NUMBER_OF_CRUISERS; i++) {
            deployShip(new Cruiser(random.nextBoolean(), board));
        }

        for (int i = 0; i < Setting.NUMBER_OF_DESTROYERS; i++) {
            deployShip(new Destroyer(random.nextBoolean(), board));
        }

        for (int i = 0; i < Setting.NUMBER_OF_GUNBOATS; i++) {
            deployShip(new Gunboat(random.nextBoolean(), board));
        }

        for (int i = 0; i < board.getSize(); i++) {
            for (int j = 0; j < board.getSize(); j++) {
                if (board.getBoard()[i][j].equals(".")) {
                    board.getBoard()[i][j] = " ";
                }
            }
        }

    }

    public void deployShip(Ship ship) {

        boolean isShipDeployed = false;

        do {
            int x = random.nextInt(10) + 1;
            int y = random.nextInt(10) + 1;
            if (ship.isEnoughSpace(x, y)) {
                ship.placeShip(x, y);
                isShipDeployed = true;
            }
        } while (isShipDeployed == false);
    }

    public void shoot(Player shooter, Player enemy) {
        boolean shooting = true;
        do {
            int x, y;
            if (shooter.isComputerControlled()) {
                int[] tab = computerShoot(shooter);
                x = tab[0];
                y = tab[1];
            } else {
                String line = scanLine();
                x = convertToXCoordinate(line);
                y = convertToYCoordinate(line);
            }
            if (isInputCorrect(x, y)) {
                if (enemy.getMyBoard().getBoard()[x][y].equals("O")) {
                    try {
                        Ship ship = findShip(x, y, enemy.getMyBoard());
                        markHit(x, y, shooter, enemy, true, false);
                        System.out.println("Hit! " + shooter.getName() + " hit a " + ship.getName());
                        ship.setHp(ship.getHp() - 1);
                        enemy.setHp(enemy.getHp() - 1);
                        if (ship.isAlive() == false) {
                            markHit(x, y, shooter, enemy, true, true);
                            System.out.println(shooter.getName() + " sunk the " + ship.getName());
                        }
                    } catch (Exception e) {
                        System.out.println("ERROR! Ship not found!");
                    }
                } else {
                    markHit(x, y, shooter, enemy, false, false);
                    System.out.println(shooter.getName() + " missed!");
                    shooting = false;
                }
            } else {
                System.out.println("Invalid input coordinates! Try again!");
            }
            if (!shooter.isComputerControlled()) {
                viewer.displayBoards(shooter);
            }
        } while (shooting && enemy.getHp() > 0);
    }

    public int[] computerShoot(Player shooter) {
        int x, y;
        for (int i = 1; i <= Setting.BOARD_SIZE; i++) {
            for (int j = 1; j <= Setting.BOARD_SIZE; j++) {
                if (shooter.getEnemyBoard().getBoard()[i][j].equals("X")) {
                    if (shooter.getEnemyBoard().getBoard()[i + 1][j].equals(" ") && i < Setting.BOARD_SIZE) {
                        return new int[]{i + 1, j};
                    } else if (shooter.getEnemyBoard().getBoard()[i][j + 1].equals(" ") && j < Setting.BOARD_SIZE) {
                        return new int[]{i, j + 1};
                    } else if (shooter.getEnemyBoard().getBoard()[i - 1][j].equals(" ") && i > 2) {
                        return new int[]{i - 1, j};
                    } else if (shooter.getEnemyBoard().getBoard()[i][j - 1].equals(" ") && j > 2) {
                        return new int[]{i, j - 1};
                    }
                }
            }
        }
        boolean alreadyHit = true;
        do {
            x = random.nextInt(10) + 1;
            y = random.nextInt(10) + 1;
            if (shooter.getEnemyBoard().getBoard()[x][y].equals(" ")) {
                return new int[]{x, y};
            }
        } while (alreadyHit);
        return new int[]{x, y};
    }

    public void markHit(int x, int y, Player shooter, Player enemy, boolean isHit, boolean isSunk) {
        if (isHit) {
            shooter.getEnemyBoard().getBoard()[x][y] = "X";
            shooter.getEnemyBoard().getBoard()[x - 1][y - 1] = ".";
            shooter.getEnemyBoard().getBoard()[x + 1][y - 1] = ".";
            shooter.getEnemyBoard().getBoard()[x + 1][y + 1] = ".";
            shooter.getEnemyBoard().getBoard()[x - 1][y + 1] = ".";
            enemy.getMyBoard().getBoard()[x][y] = "X";
        } else {
            if (shooter.getEnemyBoard().getBoard()[x][y].equals(" ")) {
                shooter.getEnemyBoard().getBoard()[x][y] = ".";
                enemy.getMyBoard().getBoard()[x][y] = ".";
            }
        }
        if (isSunk) {
            Ship ship = findShip(x, y, enemy.getMyBoard());
            if (ship.getSize() == 1) {
                shooter.getEnemyBoard().getBoard()[x][y - 1] = ".";
                shooter.getEnemyBoard().getBoard()[x][y + 1] = ".";
                shooter.getEnemyBoard().getBoard()[x - 1][y] = ".";
                shooter.getEnemyBoard().getBoard()[x + 1][y] = ".";
            } else {
                if (ship.getOrientation()) { // Pionowo
                    for (int i = 0; i < ship.getSize() + 1; i++) {
                        if (shooter.getEnemyBoard().getBoard()[x + i][y].equals(" ")) {
                            shooter.getEnemyBoard().getBoard()[x + i][y] = ".";
                            break;
                        }
                    }
                    for (int i = 0; i < ship.getSize() + 1; i++) {
                        if (shooter.getEnemyBoard().getBoard()[x - i][y].equals(" ")) {
                            shooter.getEnemyBoard().getBoard()[x - i][y] = ".";
                            break;
                        }
                    }
                } else { // Poziomo
                    for (int i = 0; i < ship.getSize() + 1; i++) {
                        if (shooter.getEnemyBoard().getBoard()[x][y + i].equals(" ")) {
                            shooter.getEnemyBoard().getBoard()[x][y + i] = ".";
                            break;
                        }
                    }
                    for (int i = 0; i < ship.getSize() + 1; i++) {
                        if (shooter.getEnemyBoard().getBoard()[x][y - i].equals(" ")) {
                            shooter.getEnemyBoard().getBoard()[x][y - i] = ".";
                            break;
                        }
                    }
                }
            }
        }
    }

    public Ship findShip(int x, int y, Board board) {

        for (Ship ship : board.getListOfDeployedShips()) {
            for (int[] coordinates :
                    ship.getMyCoordinates()) {
                if (Arrays.equals(coordinates, new int[]{x, y})) {
                    return ship;
                }
            }
        }
        return null;
    }

    public Controller(Viewer viewer) {
        this.viewer = viewer;
    }
}

