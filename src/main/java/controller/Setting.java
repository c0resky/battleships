package controller;

public final class Setting {

    public static final int BOARD_SIZE = 10;

    public static final int NUMBER_OF_GUNBOATS = 4;
    public static final int NUMBER_OF_DESTROYERS = 3;
    public static final int NUMBER_OF_CRUISERS = 2;
    public static final int NUMBER_OF_BATTLESHIPS = 1;

    public static final int SIZE_OF_GUNBOAT = 1;
    public static final int SIZE_OF_DESTROYER = 2;
    public static final int SIZE_OF_CRUISER = 3;
    public static final int SIZE_OF_BATTLESHIP = 4;
}
